<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests;

use BO\Slim\Tests\Controller\OutputRenderer;

/**
 * @see OutputRenderer
 */
class OutputRendererTest extends Base
{
    public function testInvoke(): void
    {
        $response = $this->render(['key' => 'a string']);

        self::assertStringContainsString("a string", $response->getBody()->__toString());
    }

    public function testExceptionHandling(): void
    {
        $this->expectException(\RuntimeException::class);

        $this->render(['throw exception']);
    }
}
