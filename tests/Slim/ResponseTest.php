<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests;

use BO\Slim\Response;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    public function testWithRedirect(): void
    {
        $response = new Response();
        $redirect1 = $response->withRedirect('www.default.net');

        self::assertTrue($redirect1->hasHeader('Location'));
        self::assertSame(StatusCodeInterface::STATUS_FOUND, $redirect1->getStatusCode());

        $redirect2 = $redirect1->withRedirect('www.example.com');
        self::assertCount(1, $redirect2->getHeaders());
        self::assertSame(StatusCodeInterface::STATUS_FOUND, $redirect2->getStatusCode());

        $redirect3 = $response->withRedirect('www.default.net', StatusCodeInterface::STATUS_MOVED_PERMANENTLY);
        self::assertSame(StatusCodeInterface::STATUS_MOVED_PERMANENTLY, $redirect3->getStatusCode());
    }
}
