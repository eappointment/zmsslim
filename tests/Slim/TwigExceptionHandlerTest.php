<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests;

use BO\Slim\Exception\SessionFailed;
use BO\Slim\Headers;
use BO\Slim\Request;
use BO\Slim\Tests\Fakes\ExceptionFake;
use BO\Slim\TwigExceptionHandler;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class TwigExceptionHandlerTest extends TestCase
{
    use ProphecyTrait;

    public function testExceptionHandler(): void
    {
        TwigExceptionHandler::$logger = new TestLogger();

        $loggerProphecy = $this->prophesize(TestLogger::class);
        $loggerProphecy->critical(Argument::type('string'))->shouldBeCalled();

        \App::$log = $loggerProphecy->reveal();

        $uri = new Uri('https', 'localhost', 80, '/base/');
        $response  = \App::$slim->getResponseFactory()->createResponse();
        $request   = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        $exception = new SessionFailed('Not Found');
        $exceptionHandler = new TwigExceptionHandler();

        // test invoke()
        $res = $exceptionHandler($request, $exception, true, false, false);

        self::assertStringContainsString('something went wrong', (string) $res->getBody());

        // test withHtml()
        $res = TwigExceptionHandler::withHtml($request, $response, $exception);

        self::assertStringContainsString('something went wrong', (string) $res->getBody());
        self::assertCount(0, TwigExceptionHandler::$logger->records);

        // test sub-exception catching
        $loggerProphecy = $this->prophesize(TestLogger::class);
        $loggerProphecy
            ->critical(Argument::type('string'))
            ->shouldBeCalled()
            ->willThrow(new \Exception('sub-exception'));

        \App::$log = $loggerProphecy->reveal();

        $res = TwigExceptionHandler::withHtml($request, $response, new SessionFailed('Not Found'));

        self::assertStringContainsString('something went wrong', (string) $res->getBody());
        self::assertCount(1, TwigExceptionHandler::$logger->records);
        self::assertStringContainsString('sub-exception', TwigExceptionHandler::$logger->records[0]['message']);
    }
}
