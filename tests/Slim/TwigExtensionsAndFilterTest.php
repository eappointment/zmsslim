<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests;

use BO\Slim\Container;
use BO\Slim\TwigExtensionsAndFilter;
use PHPUnit\Framework\TestCase;
use Twig\TwigFilter;

class TwigExtensionsAndFilterTest extends TestCase
{
    public function testGetFilters(): void
    {
        $sut = new TwigExtensionsAndFilter(new Container());
        $filters = $sut->getFilters();

        self::assertIsArray($filters);

        foreach ($filters as $filter) {
            self::assertInstanceOf(TwigFilter::class, $filter);
        }
    }

    public function testDecodeEntities(): void
    {
        $sut = new TwigExtensionsAndFilter(new Container());

        self::assertSame(
            "<div> Lorem <br />\n ipsum </div> <br />",
            $sut->decodeEntities(" &lt;div&gt; Lorem \n ipsum &lt;/div&gt; \r\n ")
        );
    }

    public function testMsort(): void
    {
        $testArray = [
            ['key1' => 'aß',        'key2' => 'whatever'],
            ['key1' => 'aszendent', 'key2' => 'it takes'],
            ['key1' => 'Aszendent', 'key2' => 'ya take me to the top'],
        ];
        $expected1 = [
            0 => ['key1' => 'Aszendent', 'key2' => 'ya take me to the top'],
            1 => ['key1' => 'aszendent', 'key2' => 'it takes'],
            2 => ['key1' => 'aß',        'key2' => 'whatever'],
        ];
        $expected2 = [
            0 => ['key1' => 'aszendent', 'key2' => 'it takes'],
            1 => ['key1' => 'aß',        'key2' => 'whatever'],
            2 => ['key1' => 'Aszendent', 'key2' => 'ya take me to the top'],
        ];

        $sut = new TwigExtensionsAndFilter(new Container());
        self::assertSame([], $sut->msort([], 'key1'));

        $sorted = $sut->msort($testArray, 'key1');
        self::assertSame($expected1, $sorted);

        $sorted = $sut->msort($testArray, ['key1', 'key2']);
        self::assertSame($expected1, $sorted);

        $sorted = $sut->msort($testArray, ['key2', 'key1']);
        self::assertSame($expected2, $sorted);
    }

    public function testGetObjectName(): void
    {
        $container = new Container();
        $sut = new TwigExtensionsAndFilter($container);

        self::assertSame('Container', $sut->getObjectName($container));
    }
}
