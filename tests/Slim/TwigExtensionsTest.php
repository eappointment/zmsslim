<?php

namespace BO\Slim\Tests;

use BO\Slim\Headers;
use BO\Slim\Helper\FileContentsWrapper;
use BO\Slim\Profiler;
use BO\Slim\Request;
use BO\Slim\Tests\Fakes\DldbListFake;
use BO\Slim\TwigExtension;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class TwigExtensionsTest extends TestCase
{
    use ProphecyTrait;
    public function testBasic()
    {
        $twigExtensionsClass = $this->getExtensionsClass();

        $this->assertEquals('boslimExtension', $twigExtensionsClass->getName());
        $this->assertTrue($twigExtensionsClass->isNumeric(5));
        $this->assertFalse($twigExtensionsClass->isNumeric('test'));
        $this->assertEquals(\App::$now, $twigExtensionsClass->getNow());
        \App::$now = null;
        $this->assertEquals(
            (new \DateTimeImmutable())->format('yy-mm-dd'),
            $twigExtensionsClass->getNow()->format('yy-mm-dd')
        );
        $this->assertFalse($twigExtensionsClass->getSystemStatus('APP_ENV'));
        $this->assertEquals('unittest', $twigExtensionsClass->toTextFormat('<span>unit<br />test</span>'));
        $this->assertEquals(
            '/unittest/123/?lang=en',
            $twigExtensionsClass->urlGet('getroute', ['id' => 123], ['lang' => 'en'])
        );
    }

    public function testCurrentRouteEn()
    {
        \App::$slim->getContainer()['currentRoute'] = 'unittest';
        \App::$slim->getContainer()['currentRouteParams'] = ['id' => 123, 'lang' => 'en'];

        $twigExtensionsClass = $this->getExtensionsClass();
        
        $this->assertEquals('unittest', $twigExtensionsClass->currentRoute('en')['name']);
        $this->assertArrayHasKey('lang', $twigExtensionsClass->currentRoute('en')['params']);
        $this->assertArrayNotHasKey('lang', $twigExtensionsClass->currentRoute('de')['params']);
    }

    public function testCurrentRouteDe()
    {
        \App::$slim->getContainer()['currentRoute'] = 'unittest';
        \App::$slim->getContainer()['currentRouteParams'] = ['id' => 123, 'lang' => 'de'];

        $twigExtensionsClass = $this->getExtensionsClass();
        
        $this->assertEquals('unittest', $twigExtensionsClass->currentRoute('de')['name']);
        $this->assertArrayNotHasKey('lang', $twigExtensionsClass->currentRoute('de')['params']);
    }

    public function testKindOfPayment(): void
    {
        $twigExtensionsClass = $this->getExtensionsClass();

        self::assertSame('', $twigExtensionsClass->kindOfPayment(42));
        self::assertSame('eccash', $twigExtensionsClass->kindOfPayment(null));
        self::assertSame('eccash', $twigExtensionsClass->kindOfPayment('0'));
        self::assertSame('nocash', $twigExtensionsClass->kindOfPayment(true));
        self::assertSame('nocash', $twigExtensionsClass->kindOfPayment('1'));
        self::assertSame('ec', $twigExtensionsClass->kindOfPayment(2));
        self::assertSame('ec', $twigExtensionsClass->kindOfPayment('2'));
        self::assertSame('cash', $twigExtensionsClass->kindOfPayment(3));
        self::assertSame('cash', $twigExtensionsClass->kindOfPayment('3'));
        self::assertSame('subscribecash', $twigExtensionsClass->kindOfPayment(4));
        self::assertSame('subscribecash', $twigExtensionsClass->kindOfPayment('4'));
    }

    public function testMisc(): void
    {
        self::assertSame(\App::$isImageAllowed, TwigExtension::isImageAllowed());
        
        $twigExtensionsClass = $this->getExtensionsClass();

        self::assertSame('Deutsch', $twigExtensionsClass->getLanguageDescriptor('de'));
    }

    public function testAzPrefixList(): void
    {
        $twigExtensionsClass = $this->getExtensionsClass();

        $serviceList = [
            ['name' => 'Wohnberechtigungsschein', 'id' => 120671],
            ['name' => 'Zweitwohnungssteuer',     'id' => 121859],
            ['name' => 'Zwangsversteigerung',     'id' => 326900],
            ['name' => 'Personalausweis',         'id' => 120703],
            ['name' => 'Eheschließung',           'id' => 318968],
            ['name' => 'Elterngeld',              'id' => 326079],
        ];
        $expectedList = [
            'E' => ['prefix' => 'E', 'sublist' => [
                ['name' => 'Eheschließung', 'id' => 318968],
                ['name' => 'Elterngeld'   , 'id' => 326079],
            ]],
            'P' => ['prefix' => 'P', 'sublist' => [
                ['name' => 'Personalausweis', 'id' => 120703],
            ]],
            'W' => ['prefix' => 'W', 'sublist' => [
                ['name' => 'Wohnberechtigungsschein', 'id' => 120671],
            ]],
            'Z' => ['prefix' => 'Z', 'sublist' => [
                1 => ['name' => 'Zwangsversteigerung', 'id' => 326900], // notice: array sorted without keys reassigned
                0 => ['name' => 'Zweitwohnungssteuer', 'id' => 121859],
            ]],
        ];

        $prefixList = $twigExtensionsClass->azPrefixList($serviceList, 'name');

        self::assertSame($expectedList, $prefixList);
    }

    public function testAzPrefixListCollator(): void
    {
        $twigExtensionsClass = $this->getExtensionsClass();

        $serviceList = [
            ['name' => 'Wohnberechtigungsschein', 'id' => 120671],
            ['name' => 'Zweitwohnungssteuer',     'id' => 121859],
            ['name' => 'Zwangsversteigerung',     'id' => 326900],
            ['name' => 'Personalausweis',         'id' => 120703],
            ['name' => 'Eheschließung',           'id' => 318968],
            ['name' => 'Elterngeld',              'id' => 326079],
        ];
        $expectedList = [
            'E' => ['prefix' => 'E', 'sublist' => [
                ['name' => 'Eheschließung', 'id' => 318968],
                ['name' => 'Elterngeld'   , 'id' => 326079],
            ]],
            'P' => ['prefix' => 'P', 'sublist' => [
                ['name' => 'Personalausweis', 'id' => 120703],
            ]],
            'W' => ['prefix' => 'W', 'sublist' => [
                ['name' => 'Wohnberechtigungsschein', 'id' => 120671],
            ]],
            'Z' => ['prefix' => 'Z', 'sublist' => [
                ['name' => 'Zwangsversteigerung', 'id' => 326900], // notice: array sorted with reassigned keys
                ['name' => 'Zweitwohnungssteuer', 'id' => 121859],
            ]],
        ];

        $prefixList = $twigExtensionsClass->azPrefixListCollator($serviceList, 'name', 'de_DE');

        self::assertSame($expectedList, $prefixList);

        $dldbListFake = new DldbListFake($serviceList);

        $prefixList = $twigExtensionsClass->azPrefixListCollator($dldbListFake, 'name', 'de_DE');

        self::assertSame($expectedList, $prefixList);
    }

    public function testRemoteInclude(): void
    {
        $res = TwigExtension::remoteInclude('ZmsEsiFoot');
        $expected = '<esi:include src="ZmsEsiFoot" />';

        self::assertSame($expected, $res);
    }

    public function testRemoteIncludeWrapper(): void
    {
        $wrapperProphecy = $this->prophesize(FileContentsWrapper::class);
        $wrapperProphecy
            ->setUri('https://example.com')
            ->shouldBeCalled();
        $wrapperProphecy
            ->setUserAgent(Argument::type('string'))
            ->shouldBeCalled();
        $wrapperProphecy
            ->readContents()
            ->shouldBeCalled()
            ->willReturn('exampleString');

        TwigExtension::$fileContentWrapper = $wrapperProphecy->reveal();
        $res = TwigExtension::remoteInclude('https://example.com', true);

        self::assertStringContainsString('exampleString', $res);
    }


    public function testGetEsiFromPath(): void
    {
        $twigExtensionsClass = $this->getExtensionsClass();

        self::assertSame(\App::$esiBaseUrl . '/en', $twigExtensionsClass->getEsiFromPath('includeUrl', 'en'));
    }

    public function testUrlFunctions(): void
    {
        $srvParams = [
            'REQUEST_URI' => '/base/',
            'SCRIPT_NAME' => '/base/index.php',
        ];

        $container = \App::$slim->getContainer();
        $uri = new Uri('https', 'localhost', 80, '/base/');
        $request   = new Request('GET', $uri, new Headers([]), [], $srvParams, new Stream(fopen('php://temp', 'wb+')));
        $container->set('request', $request);

        $twigExtensionsClass = $this->getExtensionsClass();

        self::assertSame('/base', $twigExtensionsClass->includeUrl());
        self::assertSame('/base', $twigExtensionsClass->baseUrl()); // is there any difference?
    }

    public function testDumpAppProfiler(): void
    {
        Profiler::add('This is a test.');

        $twigExtensionsClass = $this->getExtensionsClass();

        self::assertStringContainsString('App Profiles', $twigExtensionsClass->dumpAppProfiler());
        self::assertStringContainsString('This is a test.=', $twigExtensionsClass->dumpAppProfiler());
    }

    public function testGetClientHost(): void
    {
        $container = \App::$slim->getContainer();
        $uri = new Uri('https', 'localhost', 80, '/');
        $headers = new Headers(['x-forwarded-host' => 'forwardedHost']);
        $request = new Request('GET', $uri, $headers, [], [], new Stream(fopen('php://temp', 'wb+')));
        $container->set('request', $request);

        $twigExtensionsClass = $this->getExtensionsClass();

        self::assertSame('forwardedHost', $twigExtensionsClass->getClientHost());
    }

    public function testIsValueInArray(): void
    {
        $twigExtensionsClass = $this->getExtensionsClass();

        self::assertTrue($twigExtensionsClass->isValueInArray('two', 'one,two,three'));
        self::assertFalse($twigExtensionsClass->isValueInArray('zero', 'one,two,three'));
    }

    public function testCsvProperty(): void
    {
        $twigExtensionsClass = $this->getExtensionsClass();

        $serviceList = [
            ['name' => 'Wohnberechtigungsschein', 'id' => 120671],
            ['name' => 'Zweitwohnungssteuer',     'id' => 121859],
            ['name' => 'Zwangsversteigerung',     'id' => 326900],
            ['name' => 'Zwangsversteigerung',     'id' => 326900],
            ['name' => 'Personalausweis',         'id' => 120703],
            ['name' => 'Eheschließung',           'id' => 318968],
            ['name' => 'Elterngeld',              'id' => 326079],
        ];

        $expected = [
            'Wohnberechtigungsschein',
            'Zweitwohnungssteuer',
            'Zwangsversteigerung',
            'Personalausweis',
            'Eheschließung',
            'Elterngeld',
        ];

        self::assertSame(implode(',', $expected), $twigExtensionsClass->csvProperty($serviceList, 'name'));
    }

    private function getExtensionsClass(): TwigExtension
    {
        return \App::$slim
            ->getContainer()
            ->get('view')
            ->getEnvironment()
            ->getExtension('\BO\Slim\TwigExtension');
    }
}
