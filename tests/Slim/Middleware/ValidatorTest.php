<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Middleware;

use BO\Mellon\Validator;
use BO\Slim\Middleware\Validator as ValidatorMiddleware;
use BO\Slim\Request;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class ValidatorTest extends TestCase
{
    public function testInvoke()
    {
        $uri = new Uri('http', 'localhost', 80, '/admin/account/', 'parameter=value');
        $request = new Request('POST', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        $request->getBody()->write('{"attribute":"aValue"}');
        $nextHandler = new RequestHandlerMock();

        $sut = new ValidatorMiddleware();
        $sut->__invoke($request, $nextHandler);

        /** @var Validator $validator */
        $validator = $nextHandler->getRequest()->getAttribute('validator');

        self::assertInstanceOf(Validator::class, $validator);
        self::assertIsString($validator->getParameter('parameter')->isString()->assertValid()->getValue());
        self::assertIsArray($validator->getInput()->isJson()->assertValid()->getValue());

        $uri = new Uri('http', 'localhost', 80, '/admin/account/');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));

        $sut->__invoke($request, $nextHandler);
        $validator = $nextHandler->getRequest()->getAttribute('validator');

        self::assertNull($validator->getParameter('parameter')->isString()->getValue());
        self::assertNull($validator->getInput()->isJson()->getValue());
    }
}
