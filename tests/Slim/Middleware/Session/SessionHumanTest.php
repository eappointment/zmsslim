<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Middleware\Session;

use BO\Slim\Middleware\Session\SessionData;
use BO\Slim\Middleware\Session\SessionHuman;
use BO\Slim\Request;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class SessionHumanTest extends TestCase
{
    use ProphecyTrait;

    public function testWriteVerifySession(): void
    {
        $uri = new Uri('http', 'localhost', 80, '/admin');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        $sessionData = new SessionData([]);
        $sessionLoader = function () use ($sessionData) {
            return $sessionData;
        };

        $instance = SessionHuman::fromContainer($sessionLoader);

        self::assertFalse($instance->isVerified());

        $instance->writeVerifySession($request, 'test');

        self::assertTrue($sessionData->has('client', 'human'));
        self::assertTrue($sessionData->has('ts', 'human'));
        self::assertTrue($sessionData->has('origin', 'human'));
        self::assertTrue($sessionData->has('remoteAddress', 'human'));
        self::assertTrue($instance->isVerified());
    }

    public function testWriteBotSession(): void
    {
        $sessionData = new SessionData([]);
        $sessionLoader = function () use ($sessionData) {
            return $sessionData;
        };

        $instance = SessionHuman::fromContainer($sessionLoader);
        $instance->writeBotSession('bot');

        self::assertTrue($sessionData->has('client', 'human'));
        self::assertTrue($sessionData->has('ts', 'human'));
        self::assertTrue($sessionData->has('origin', 'human'));
    }

    public function testIsOverAged(): void
    {
        $sessionData = new SessionData(['human' => ['ts' => 0]]);
        $sessionLoader = function () use ($sessionData) {
            return $sessionData;
        };

        $instance = SessionHuman::fromContainer($sessionLoader);
        self::assertTrue($instance->isOverAged());
        $instance->set('ts', \App::$now->getTimestamp() + SessionHuman::MAX_TIME + 10, 'human');
        self::assertFalse($instance->isOverAged());
    }

    public function testIsUnderAged(): void
    {
        $sessionData = new SessionData(['human' => ['ts' => \App::$now->getTimestamp() + 10]]);
        $sessionLoader = function () use ($sessionData) {
            return $sessionData;
        };

        $instance = SessionHuman::fromContainer($sessionLoader);
        self::assertTrue($instance->isUnderAged());
        $instance->set('ts', \App::$now->getTimestamp() - SessionHuman::MAX_TIME, 'human');
        self::assertFalse($instance->isUnderAged());
    }

    public function testHasAddedStep(): void
    {
        $instance = $this->getEmptyInstance();
        $instance->addStep('dayselect');

        self::assertFalse($instance->hasStep('timeselect'));
        self::assertTrue($instance->hasStep('dayselect'));
        self::assertSame(['dayselect' => 1], $instance->get('step', 'human'));
        self::assertFalse($instance->hasStepMaxReload('dayselect'));

        for ($i = 0; $i <= SessionHuman::MAX_RELOAD; $i++) {
            $instance->addStep('dayselect');
        }

        self::assertTrue($instance->hasStepMaxReload('dayselect'));
    }

    public function testHasStepReloadExceeded(): void
    {
        $instance = $this->getEmptyInstance();

        self::assertFalse($instance->hasStepReloadExceeded('dayselect'));

        $instance->addStep('dayselect');

        self::assertFalse($instance->hasStepReloadExceeded('dayselect'));

        $instance->addStep('dayselect');

        self::assertTrue($instance->hasStepReloadExceeded('dayselect'));
    }

    public function testRedirectOnSuspicion(): void
    {
        $loggerProphecy = $this->prophesize(LoggerInterface::class);
        $loggerProphecy->notice(Argument::type('string'))->shouldBeCalled();
        //$loggerProphecy->error(Argument::type('string'))->shouldBeCalled();
        \App::$log = $loggerProphecy->reveal();

        $uri = new Uri('http', 'localhost', 80, '/register');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        $sessionData = new SessionData(['human' => ['step' => ['timeselect' => SessionHuman::MAX_RELOAD + 1]]]);
        $sessionLoader = function () use ($sessionData) {
            return $sessionData;
        };

        $instance = SessionHuman::fromContainer($sessionLoader);

        self::assertTrue($instance->redirectOnSuspicion($request, ['dayselect', 'timeselect'], 'testreferer'));

        $instance->addStep('dayselect');

        self::assertTrue($instance->redirectOnSuspicion($request, ['dayselect', 'timeselect'], 'testreferer'));

        $sessionData->remove('step', 'human');
        $instance->addStep('dayselect');
        $instance->addStep('timeselect');

        self::assertTrue($instance->redirectOnSuspicion($request, ['dayselect', 'timeselect'], 'testreferer'));

        $instance->set('remoteAddress', '0.0.0.0', 'human');

        self::assertTrue($instance->redirectOnSuspicion(
            $request->withAttribute('ip_address', '0.0.0.0'),
            ['dayselect', 'timeselect'],
            'testreferer'
        ));

        $instance->writeVerifySession($request->withAttribute('ip_address', '0.0.0.0'), 'captcha');

        self::assertFalse($instance->redirectOnSuspicion(
            $request->withAttribute('ip_address', '0.0.0.0'),
            ['dayselect', 'timeselect'],
            'testreferer'
        ));
    }

    private function getEmptyInstance(): SessionHuman
    {
        $sessionData = new SessionData([]);
        $sessionLoader = function () use ($sessionData) {
            return $sessionData;
        };

        return SessionHuman::fromContainer($sessionLoader);
    }
}
