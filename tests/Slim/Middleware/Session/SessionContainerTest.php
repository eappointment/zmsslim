<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Middleware\Session;

use BO\Slim\Middleware\Session\SessionContainer;
use BO\Slim\Middleware\Session\SessionData;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class SessionContainerTest extends TestCase
{
    use ProphecyTrait;

    public function testFromContainer(): void
    {
        $callback = function () {
            return [];
        };
        $instance = SessionContainer::fromContainer($callback);

        self::assertInstanceOf(SessionContainer::class, $instance);
    }

    public function testSessionHandling(): void
    {
        $sessionProphecy = $this->prophesize(SessionData::class);
        $sessionProphecy
            ->has(Argument::type('string'), Argument::type('string'))
            ->shouldBeCalled()->willReturn(false);
        $sessionProphecy
            ->get(Argument::type('string'), Argument::type('string'), null)
            ->shouldBeCalled();
        $sessionProphecy
            ->set(Argument::type('string'), Argument::any(), Argument::type('string'))
            ->shouldBeCalled();
        $sessionProphecy
            ->setGroup(Argument::type('array'), Argument::type('bool'))
            ->shouldBeCalled();
        $sessionProphecy
            ->clearGroup(Argument::type('string'))
            ->shouldBeCalled();
        $sessionProphecy
            ->remove(Argument::type('string'), Argument::type('string'))
            ->shouldBeCalled();
        $sessionProphecy->getEntity()->shouldBeCalled()->willReturn(new \ArrayObject());

        $sessionProphecy->writeData()->shouldBeCalled();
        $sessionProphecy->clear()->shouldBeCalled();
        $sessionProphecy->restart()->shouldBeCalled();
        $sessionProphecy->isEmpty()->shouldBeCalled();
        $sessionProphecy->jsonSerialize()->shouldBeCalled()->willReturn('{}');

        $sessionMock = $sessionProphecy->reveal();
        $callback = function () use ($sessionMock) {
            return $sessionMock;
        };
        $instance = SessionContainer::fromContainer($callback);
        $instance->setGroup(['human' => ['step' => 'dayselect']], true);
        $instance->remove('step', 'human');
        $instance->set('step', 'timeselect', 'human');
        $instance->has('step', 'human');
        $instance->get('step', 'human');
        $instance->clearGroup('human');
        $instance->clear();
        $instance->restart();
        $instance->isEmpty();
        $instance->getEntity();
        $instance->jsonSerialize();
        $instance->writeData();
    }
}
