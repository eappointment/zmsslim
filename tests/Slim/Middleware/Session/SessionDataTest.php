<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Middleware\Session;

use BO\Slim\Exception\SessionLocked;
use BO\Slim\Middleware\Session\SessionData;
use PHPUnit\Framework\TestCase;

class SessionDataTest extends TestCase
{
    public function testWriteData(): void
    {
        $this->expectException(SessionLocked::class);

        $sut = new SessionData([]);
        session_start();
        $sut->writeData();
        $sut->set('key', 'value');
    }

    public function testSetAndGetEntity(): void
    {
        $data = ['key' => 'value'];
        $sut = new SessionData($data);

        try {
            $sut->getEntity();
        } catch (\Exception $exception) {
            self::assertSame('Entity-Class not set', $exception->getMessage());
        }

        $sut->setEntityClass(new \stdClass());
        self::assertSame($data, $sut->getEntity()->content);
    }

    public function testMultipleFunctions(): void
    {
        $sut = new SessionData([]);
        $sut->set('key', 'value');
        $sut->setGroup(['group' => ['member' => 'one']], true);

        self::assertSame('{"key":"value","group":{"member":"one"}}', $sut->jsonSerialize());
        self::assertSame('default', $sut->get('misstaken', null, 'default'));

        $sut->remove('key');
        $sut->clearGroup('group');
        self::assertFalse($sut->isEmpty());

        $sut->remove('group');
        self::assertTrue($sut->isEmpty());
    }

    public function testRestart(): void
    {
        session_start();
        $id = session_id();
        $sut = new SessionData(['key' => 'value']);
        $sut->restart();

        self::assertTrue($sut->isEmpty());
        self::assertNotSame($id, session_id());
    }

    public function testClear(): void
    {
        session_start();
        $sut = new SessionData([]);
        $sut->clear();

        self::assertEmpty(session_id());
    }
}
