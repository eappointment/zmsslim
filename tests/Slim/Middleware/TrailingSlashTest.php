<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Middleware;

use BO\Slim\Middleware\TrailingSlash;
use BO\Slim\Request;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class TrailingSlashTest extends TestCase
{
    public function testInvoke()
    {
        $uri = new Uri('http', 'localhost', 80, '/admin');
        $res = fopen('php://temp', 'wb+');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream($res));
        $nextHandler = new RequestHandlerMock();

        $sut = new TrailingSlash();
        $response = $sut->__invoke($request, $nextHandler);

        self::assertTrue($response->hasHeader('Location'));
        self::assertSame(StatusCodeInterface::STATUS_MOVED_PERMANENTLY, $response->getStatusCode());
        self::assertContains('//localhost/admin/', $response->getHeader('Location'));

        $request  = new Request('GET', $uri, new Headers(['X-Ssl' => 'yes']), [], [], new Stream($res));
        $response = $sut($request, $nextHandler);
        self::assertSame('https://localhost:80/admin/', $response->getHeader('Location')[0]);

        fclose($res);
    }
}
