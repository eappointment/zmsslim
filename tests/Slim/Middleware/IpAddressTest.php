<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Middleware;

use BO\Slim\Middleware\IpAddress;
use BO\Slim\Request;
use BO\Slim\Response;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class IpAddressTest extends TestCase
{
    public function testInvoke()
    {
        $uri = new Uri('http', 'localhost', 80, '/admin/account/');
        $serverParams = [
            'REMOTE_ADDR' => '0.12.34.56',
        ];
        $request = new Request('GET', $uri, new Headers([]), [], $serverParams, new Stream(fopen('php://temp', 'wb+')));
        $nextHandler = new RequestHandlerMock();

        $sut = new IpAddress(true);

        $sut->__invoke($request, $nextHandler);

        self::assertSame('0.12.34.56', $nextHandler->getRequest()->getAttribute('ip_address'));
        self::assertInstanceOf(Response::class, $sut($request, null));

        $headers = new Headers(['X-Forwarded' => '::dead:beef:10.20.30.40', 'X-Remote-Ip' => '::bad::idea']);
        $request = new Request('GET', $uri, $headers, [], $serverParams, new Stream(fopen('php://temp', 'wb+')));
        $sut = new IpAddress(
            true,
            [],
            'ip_address',
            ['X-Remote-Ip', 'X-Forwarded-For', 'X-Forwarded', 'X-Cluster-Client-Ip', 'Client-Ip',]
        );

        $sut($request, $nextHandler);
        self::assertSame('::dead:beef:10.20.30.40', $nextHandler->getRequest()->getAttribute('ip_address'));
    }
}
