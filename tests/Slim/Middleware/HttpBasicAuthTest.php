<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Middleware;

use BO\Slim\Middleware\HttpBasicAuth;
use BO\Slim\Request;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class HttpBasicAuthTest extends TestCase
{
    public function testGetInfo(): void
    {
        $uri = new Uri('http', 'localhost', 80, '/admin');
        $request = new Request(
            'GET',
            $uri,
            new Headers([]),
            [],
            ['PHP_AUTH_USER' => 'foo','PHP_AUTH_PW' => 'bar'],
            new Stream(fopen('php://temp', 'wb+'))
        );

        $nextHandler = new RequestHandlerMock();

        $sut = new HttpBasicAuth(HttpBasicAuth::useAppConfig());
        $response = $sut(
            $request,
            $nextHandler
        );

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());

        \App::$httpBasicAuth = ['foo' => 'g5z3gj5g3j4g5j36g'];
        $response = $sut(
            $request,
            $nextHandler
        );

        self::assertSame(StatusCodeInterface::STATUS_UNAUTHORIZED, $response->getStatusCode());

        \App::$httpBasicAuth = ['foo' => password_hash('bar', PASSWORD_DEFAULT)];
        $response = $sut(
            $request,
            $nextHandler
        );

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());

        $response = $sut(
            new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+'))),
            $nextHandler
        );

        self::assertSame(StatusCodeInterface::STATUS_UNAUTHORIZED, $response->getStatusCode());
    }
}
