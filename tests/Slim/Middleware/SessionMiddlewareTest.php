<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Middleware;

use BO\Slim\Middleware\Session\SessionContainer;
use BO\Slim\Middleware\Session\SessionData;
use BO\Slim\Middleware\SessionMiddleware;
use BO\Slim\Request;
use BO\Slim\Request as ZmsSlimRequest;
use BO\Slim\Response;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;
use stdClass;

class SessionMiddlewareTest extends TestCase
{
    public function testInvoke(): void
    {
        $sut = new SessionMiddleware(\App::SESSION_NAME, new stdClass());
        $uri = new Uri('http', 'localhost', 80, '/admin');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        $nextHandler = new RequestHandlerMock();

        self::assertInstanceOf(SessionData::class, $sut->getSessionContainer($request));
        $sut($request, $nextHandler);

        self::assertInstanceOf(ZmsSlimRequest::class, $nextHandler->getRequest());
        self::assertInstanceOf(SessionContainer::class, $nextHandler->getRequest()->getAttribute('session'));
        self::assertInstanceOf(Response::class, $sut($request, null));
    }
}
