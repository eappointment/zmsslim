<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Middleware;

use BO\Slim\Middleware\SessionHeadersHandler;
use BO\Slim\Request;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class SessionHeadersHandlerTest extends TestCase
{
    public function testInvoke(): void
    {
        $sut = new SessionHeadersHandler();
        $uri = new Uri('http', 'localhost', 80, '/admin');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        $nextHandler = new RequestHandlerMock();

        session_name('test');
        session_id('a1s2d3f4g5h6');

        $response = $sut($request, $nextHandler);

        self::assertTrue($response->hasHeader('Set-Cookie'));
        self::assertTrue($response->hasHeader('Expires'));
        self::assertTrue($response->hasHeader('Cache-Control'));
        self::assertTrue($response->hasHeader('Pragma'));
        self::assertContains('test=a1s2d3f4g5h6; path=/', $response->getHeader('Set-Cookie'));

        $request = new Request(
            'GET',
            $uri,
            new Headers([]),
            ['test' => 'a1s2d3f4g5h6'],
            [],
            new Stream(fopen('php://temp', 'wb+'))
        );

        $response = $sut($request, $nextHandler);

        self::assertfalse($response->hasHeader('Set-Cookie'));
    }

    public function testCookieParams(): void
    {
        $sut = new SessionHeadersHandler();
        $uri = new Uri('http', 'localhost', 80, '/admin');
        $request = new Request('GET', $uri, new Headers([]), ['a' => 'b'], [], new Stream(fopen('php://temp', 'wb+')));
        $nextHandler = new RequestHandlerMock();

        session_id('a1s2d3f4g5h6');
        session_start([
            'cookie_lifetime'   => 600, //lifetime
            'cookie_domain'     =>'.domain.net',
            'cookie_path'       => '/path',
            'cookie_secure'     => true, //secure
            'cookie_httponly'   => true //httponly
        ]);

        $response = $sut($request, $nextHandler);

        self::assertStringContainsString(
            'max-age=600; domain=.domain.net; path=/path; secure; httponly',
            $response->getHeader('Set-Cookie')[0]
        );
        self::assertStringContainsString(
            'expires=',
            $response->getHeader('Set-Cookie')[0]
        );
    }

    public function testCacheLimiterPublic(): void
    {
        $handler  = new SessionHeadersHandler('public');

        session_id('a1s2d3f4g5h6');
        $response = $handler($this->getDefaultRequest(), new RequestHandlerMock());

        self::assertTrue($response->hasHeader('Expires'));
        self::assertTrue($response->hasHeader('Cache-Control'));
        self::assertTrue($response->hasHeader('Last-Modified'));
        self::assertStringContainsString('public', $response->getHeader('Cache-Control')[0]);
    }

    public function testCacheLimiterPrivateNoExpire(): void
    {
        $handler  = new SessionHeadersHandler('private_no_expire');

        session_id('a1s2d3f4g5h6');
        $response = $handler($this->getDefaultRequest(), new RequestHandlerMock());

        self::assertTrue($response->hasHeader('Cache-Control'));
        self::assertTrue($response->hasHeader('Last-Modified'));
    }

    public function testCacheLimiterPrivate(): void
    {
        $handler  = new SessionHeadersHandler('private');

        session_id('a1s2d3f4g5h6');
        $response = $handler($this->getDefaultRequest(), new RequestHandlerMock());

        self::assertTrue($response->hasHeader('Expires'));
        self::assertTrue($response->hasHeader('Cache-Control'));
        self::assertTrue($response->hasHeader('Last-Modified'));
    }

    public function testCacheLimiterNocache(): void
    {
        $handler  = new SessionHeadersHandler('nocache');

        session_id('a1s2d3f4g5h6');
        $response = $handler($this->getDefaultRequest(), new RequestHandlerMock());

        self::assertTrue($response->hasHeader('Expires'));
        self::assertTrue($response->hasHeader('Cache-Control'));
        self::assertTrue($response->hasHeader('Pragma'));
        self::assertStringContainsString(
            'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
            $response->getHeader('Cache-Control')[0]
        );
        self::assertStringContainsString('no-cache', $response->getHeader('Pragma')[0]);
    }

    protected function getDefaultRequest(): Request
    {
        $uri = new Uri('http', 'localhost', 80, '/admin');
        return new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
    }
}
