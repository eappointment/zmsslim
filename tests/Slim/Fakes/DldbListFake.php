<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Fakes;

/**
 * fake class to avoid repo dependencies with zmsdldb
 */
class DldbListFake extends \ArrayObject
{
    /**
     * warning: this function is part of zmsdldb and is used within the TwigExtension class,
     * assuming that the list-object is of type BO\Dldb\Collection\Base
     */
    public function sortWithCollator(): DldbListFake
    {
        $resultList = clone $this;
        $resultList->uasort(function ($a, $b) {
            return strnatcmp($a['name'], $b['name']);
        });

        return $resultList;
    }
}
