<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests;

use BO\Slim\ErrorHandler;
use BO\Slim\Request;
use BO\Slim\Response;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class ErrorHandlerTest extends TestCase
{
    public function testCheck(): void
    {
        $uri = new Uri('http', 'localhost', 80, '/admin');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        $handler = new ErrorHandler($request, new Response());

        $result = $handler->check();
        self::assertFalse($result);

        $callableWithoutQueryParams = function (ErrorHandler $handler) {
            return ['route' => 'getroute', 'params' => ['id' => 1, 'lang' => 'de']];
        };

        $result = $handler->check($callableWithoutQueryParams);

        self::assertInstanceOf(Response::class, $result);
        self::assertTrue($result->hasHeader('Location'));
        self::assertSame('/unittest/1/de/', $result->getHeader('Location')[0]);

        $callableWithQueryParams = function (ErrorHandler $handler) {
            return [
                'route' => 'postroute',
                'queryParams' => ['id' => 1, 'lang' => 'de'],
            ];
        };

        $result = $handler->check($callableWithQueryParams);

        self::assertInstanceOf(Response::class, $result);
    }
}
