<?php
namespace BO\Slim\Tests;

use BO\Slim\Helper\FileContentsWrapper;
use BO\Slim\TwigExtension;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class FileContentsWrapperTest extends TestCase
{
    use ProphecyTrait;
    private $fileContentsWrapper;
    private $useragent;
    private $uri = "https://example.com";

    protected function setUp(): void
    {
        $this->fileContentsWrapper = new FileContentsWrapper();
        $this->fileContentsWrapper->setUri($this->uri);
        $this->useragent = $this->fileContentsWrapper->getUserAgent();
    }

    public function testSetUri(): void
    {
        self::assertSame($this->uri, $this->fileContentsWrapper->getUri());

        $newUri = 'https://example.org';
        $this->fileContentsWrapper->setUri($newUri);
        self::assertSame($newUri, $this->fileContentsWrapper->getUri());
    }

    public function testSetUriWithException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->fileContentsWrapper->setUri('');
    }

    public function testGetUserAgent(): void
    {
        $expectedDefaultUseragent = "ZMS";
        $options = stream_context_get_options($this->fileContentsWrapper->getContext());
        self::assertSame($expectedDefaultUseragent, $this->useragent);
        self::assertStringContainsString($expectedDefaultUseragent, $options['http']['header']);

        $newUseragent = "Client-Slim-ENV";
        $this->fileContentsWrapper->setUserAgent($newUseragent);
        $options = stream_context_get_options($this->fileContentsWrapper->getContext());
        self::assertSame("Client-Slim-ENV", $this->fileContentsWrapper->getUserAgent());
        self::assertStringContainsString($newUseragent, $options['http']['header']);
    }

    public function testGetContext(): void
    {
        $options = stream_context_get_options($this->fileContentsWrapper->getContext());
        $expectedOptions = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-language: de\r\n" .
                    "Cookie: zms=development\r\n" .
                    "user-agent: $this->useragent \r\n"
            )
        );
        self::assertSame($expectedOptions, $options);
    }

    public function testGetContents(): void
    {
        self::assertStringContainsString(
            'This domain is for use in illustrative examples in documents.',
            $this->fileContentsWrapper->readContents()
        );
    }

    public function testGetMethod(): void
    {
        self::assertSame("GET", $this->fileContentsWrapper->getMethod());
        $options = stream_context_get_options($this->fileContentsWrapper->getContext());
        self::assertSame("GET", $options['http']['method']);
        $this->fileContentsWrapper->setMethod("POST");
        self::assertSame("POST", $this->fileContentsWrapper->getMethod());

        $options = stream_context_get_options($this->fileContentsWrapper->getContext());
        self::assertSame("POST", $options['http']['method']);
    }

    public function testGetUri(): void
    {
        self::assertSame($this->uri, $this->fileContentsWrapper->getUri());
    }

    public function testChangeUri(): void
    {
        self::assertSame($this->uri, $this->fileContentsWrapper->getUri());

        $newUri = "https://example.org";
        $this->fileContentsWrapper->setUri($newUri);
        self::assertSame($newUri, $this->fileContentsWrapper->getUri());
    }

    public function testReadContentsWithNewUri(): void
    {
        $this->fileContentsWrapper->setUri("https://example.org");
        self::assertStringContainsString(
            "This domain is for use in illustrative examples in documents.",
            $this->fileContentsWrapper->readContents()
        );
    }

    public function testReadContentsWithEmptyUriInConstructor(): void
    {
        $newWrapper = new FileContentsWrapper('');
        $this->expectException(InvalidArgumentException::class);
        $newWrapper->readContents();
    }

    public function testReadContentsWithDefaultUri(): void
    {
        $newWrapper = new FileContentsWrapper();
        $this->expectException(InvalidArgumentException::class);
        $newWrapper->readContents();
    }

    public function testSetCookie(): void
    {
        $defaultCookie = "zms=development";
        $options = stream_context_get_options($this->fileContentsWrapper->getContext());
        self::assertSame($defaultCookie, $this->fileContentsWrapper->getCookie());
        self::assertStringContainsString($defaultCookie, $options['http']['header']);

        $newCookie = "cookie";
        $this->fileContentsWrapper->setCookie($newCookie);
        $options = stream_context_get_options($this->fileContentsWrapper->getContext());
        self::assertSame($newCookie, $this->fileContentsWrapper->getCookie());
        self::assertStringContainsString($newCookie, $options['http']['header']);
    }
}
