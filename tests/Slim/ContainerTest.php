<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests;

use BO\Slim\Container;
use BO\Slim\Exception\UnknownIdentifierException;
use BO\Slim\Factory\ResponseFactory;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    public function testSetHasGet(): void
    {
        $sut = new Container();
        $sut->set('zmsslim.service.response-factory', new ResponseFactory());

        self::assertTrue($sut->has('zmsslim.service.response-factory'));
        self::assertFalse($sut->has('zmsslim.service.time-machine'));

        self::assertInstanceOf(ResponseFactory::class, $sut->get('zmsslim.service.response-factory'));
    }

    public function testGetFails(): void
    {
        $this->expectException(UnknownIdentifierException::class);

        $sut = new Container();
        $sut->set('zmsslim.service.response-factory', new ResponseFactory());
        $sut->get('zmsslim.service.time-machine');
    }
}
