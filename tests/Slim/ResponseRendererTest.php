<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests;

use BO\Slim\Tests\Controller\ResponseRenderer;

/**
 * @see ResponseRenderer
 */
class ResponseRendererTest extends Base
{
    public function testInvoke(): void
    {
        $response = $this->render(['key' => 'an argument'], ['key' => 'parameter']);

        self::assertStringContainsString("'an argument'", $response->getBody()->__toString());
    }
}
