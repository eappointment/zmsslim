<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Controller;

use BO\Slim\Controller;
use BO\Slim\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * class to test the plain controller behavior of returning a response
 */
class ResponseRenderer extends Controller
{
    public static function render(): ResponseInterface
    {
        $arguments = func_get_args();
        $response  = new Response();

        $response->getBody()->write(var_export($arguments, true));

        return $response;
    }
}
