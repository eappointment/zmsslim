<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Controller;

use BO\Slim\Container;
use BO\Slim\Controller\TwigExceptionViewer;
use BO\Slim\Request;
use BO\Slim\Response;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class TwigExceptionViewerTest extends TestCase
{
    use ProphecyTrait;

    public function testInvoke(): void
    {
        $uri = new Uri('http', 'localhost', 80, '/admin');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        $arguments = ['message' => 'test-message', 'template' => 'Exception'];
        $controller = new TwigExceptionViewer(new Container([]));
        $loggerProphecy = $this->prophesize(LoggerInterface::class);
        $loggerProphecy->critical(Argument::type('string'))->shouldBeCalled();

        \App::$log = $loggerProphecy->reveal();
        $response = $controller($request, new Response(), $arguments);


        self::assertStringContainsString('something went wrong', $response->getBody()->__toString());
    }
}
