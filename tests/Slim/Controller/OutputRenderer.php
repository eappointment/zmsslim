<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Controller;

use BO\Slim\Controller;

class OutputRenderer extends Controller
{
    public static function render()
    {
        $args = func_get_args();
        $args = $args[0];

        echo 'a string';

        if (in_array('throw exception', $args)) {
            throw new \RuntimeException('an exception');
        }
    }
}
