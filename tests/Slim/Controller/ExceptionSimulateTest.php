<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests\Controller;

use BO\Slim\Container;
use BO\Slim\Controller\ExceptionSimulate;
use BO\Slim\Request;
use BO\Slim\Response;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class ExceptionSimulateTest extends TestCase
{
    public function testInvoke(): void
    {
        $this->expectException(\Exception::class);

        $uri = new Uri('http', 'localhost', 80, '/admin');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        $controller = new ExceptionSimulate(new Container([]));
        $controller($request, new Response(), []);
    }
}
