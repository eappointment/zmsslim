<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Tests;

use BO\Slim\Version;
use PHPUnit\Framework\TestCase;

class VersionTest extends TestCase
{
    public function testGetArray(): void
    {
        if (!file_exists(\App::APP_PATH . '/VERSION')) {
            self::assertSame(['major' => 'unknown', 'minor' => '0', 'patch' => '0',], Version::getArray());
        }

        if (file_put_contents(\App::APP_PATH . '/VERSION', 'v12.34.56')) {
            self::assertSame(['major' => '12', 'minor' => '34', 'patch' => '56',], Version::getArray());
            unlink(\App::APP_PATH . '/VERSION');
        }
    }
}
