## 2.28.01
* Rendering templates can use application configuration objects

## 2.27.0
* Compatibility with PHP80 established and set as minimum requirement for composer

## 2.26.09
* #57629 - A middleware was introduced to manage Cors headers

## 2.26.00
* #36401 - Request service/helper class added

## 2.25.04
* #57413 - remoteInclude method was customized with a helper class to be able to extend the unit tests.

## 2.25.03
* #57380 - add new method to check if max reload of single step is exceeded

## 2.25.01
* #36493 - add application maintenance basics

## 2.24.08
* The php function "strftime()" will be deprecated from php version 8. The "IntlDateFormatter" class has now been used to format dates.

## 2.4.9
* #51124 Mehrspachigkeit: Es können nun verschiedene Übersetzungsdateien (po, json) verwendet werden und wenn Mehrsprachigkeit deaktiviert ist, liefern Sprachfunktionen deutsch als default zurück
* #51864 Captcha-Verifizierung: Nach erfolgreicher Verifizierung wird man nun an die verweisende URL zurück geschickt

## 2.4.5
* #49629 Sicherheit: Aktualisierung zentraler Bibliotheken für Stabilität und Sicherheit des Systems durchgeführt

## 2.4.4
* #46558 Bugfix: Wenn ESI Includes deaktiviert sind werden die Inhalte nun mit einem User-Agent im Request abgefragt wodurch die Requests nicht mehr blockiert werden
* #46686 Bugfix: Doppelte max-age und expires cache Einträge wurden korrigiert

## 2.4.3
* #36048 - Kompatibilität zu altem d115mandant Repo korrigiert

## 2.4.2
* #38421 - Nutze git HEAD als hash wenn kein benannter ref vorhanden ist
* Der Name des Systemnutzers wurde zum Twig Cache-Pfad hinzugefügt, um Rechte-Probleme zu vermeiden
* #36523 Erlaube mehr Reloads bevor ein Captcha angezeigt wird
* #35794 Bugfix: Beim Sessionhandling wird zum Ändern von gruppierten Sessiondaten geprüft ob Daten vorhanden sind