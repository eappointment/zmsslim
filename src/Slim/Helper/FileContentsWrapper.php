<?php

namespace BO\Slim\Helper;

use InvalidArgumentException;

class FileContentsWrapper
{
    protected $uri = '';
    protected $useragent = '';
    protected $cookie = '';
    protected $method = '';
    protected $context;

    public function __construct(
        string $uri = '',
        string $method = 'GET',
        string $useragent = 'ZMS',
        string $cookie = 'zms=development'
    ) {
        $this->uri = $uri;
        $this->method = $method;
        $this->useragent = $useragent;
        $this->cookie = $cookie;
        $this->setContext();
    }

    private function setContext()
    {
        $options = [
            'http' => [
                'method' => $this->method,
                'header' => "Accept-language: de\r\n" .
                    "Cookie: " . $this->cookie . "\r\n" .
                    "user-agent: " . $this->useragent . " \r\n"
            ]
        ];
        if (getenv('HTTP_PROXY') !== false) {
            $options['http']['proxy'] = getenv('HTTP_PROXY');
        }
        $this->context = stream_context_create($options);
    }

    public function setUserAgent(string $useragent)
    {
        $this->useragent = $useragent;
        $this->setContext();
    }

    public function setMethod(string $method)
    {
        $this->method = $method;
        $this->setContext();
    }

    public function setCookie(string $cookie)
    {
        $this->cookie = $cookie;
        $this->setContext();
    }

    private function checkForEmptyURI(string $uri)
    {
        if (empty($uri)) {
            throw new InvalidArgumentException('URI is a required input');
        }
    }

    public function setUri(string $uri)
    {
        $this->checkForEmptyURI($uri);
        $this->uri = $uri;
    }

    public function getUserAgent(): string
    {
        return $this->useragent;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getCookie(): string
    {
        return $this->cookie;
    }

    public function readContents(): string
    {
        // If URI was set via constructor, this is the only chance to catch it being an empty string.
        $this->checkForEmptyURI($this->uri);
        return file_get_contents($this->uri, false, $this->context);
    }
}
