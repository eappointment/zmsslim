<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Slim\Helper;

use Slim\Psr7\Factory\UriFactory;

class Request
{
    /**
     * @return int|string|null
     */
    public static function readParameterFromRequest($request, $parameterName, $default = null)
    {
        $value = $default;
        $queryParams  = $request->getQueryParams();
        $serverParams = $request->getServerParams();
        if (isset($queryParams[$parameterName])) {
            $value = $queryParams[$parameterName];
        } elseif (isset($serverParams[$parameterName])) {
            $value = $serverParams[$parameterName];
        } elseif (isset($serverParams['HTTP_REFERER'])) {
            $referer = (new UriFactory)->createUri($serverParams['HTTP_REFERER']);
            parse_str($referer->getQuery(), $queryParams);

            if (isset($queryParams[$parameterName])) {
                $value = $queryParams[$parameterName];
            }
        }

        return $value;
    }
}
