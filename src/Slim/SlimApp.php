<?php

namespace BO\Slim;

use Psr\Container\ContainerInterface;
use Slim\Interfaces\RouteCollectorInterface;

class SlimApp extends \Slim\App
{
    public function urlFor(string $name, array $params = []): string
    {
        /** @var RouteCollectorInterface $router */
        $router = $this->getContainer()->get('router');
        return $router->getRouteParser()->urlFor($name, $params);
    }

    /**
     * @SuppressWarnings("PHPMD.Superglobals")
     *
     * @return void
     */
    public function determineBasePath(): void
    {
        $basePath = '';
        $serverParams = $_SERVER;

        if (!isset($serverParams['REQUEST_URI']) || !isset($serverParams['SCRIPT_NAME'])) {
            return;
        }

        while (min(strlen($serverParams['REQUEST_URI']), strlen($serverParams['SCRIPT_NAME'])) > strlen($basePath)
            && strncmp($serverParams['REQUEST_URI'], $serverParams['SCRIPT_NAME'], strlen($basePath) + 1) === 0
        ) {
            $basePath = substr($serverParams['REQUEST_URI'], 0, strlen($basePath) + 1);
        }

        $this->setBasePath(rtrim($basePath, '/'));
    }

    /**
     * @return Container|null
     */
    public function getContainer(): ?ContainerInterface
    {
        return parent::getContainer();
    }
}
