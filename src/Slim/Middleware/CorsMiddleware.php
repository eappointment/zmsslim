<?php

namespace BO\Slim\Middleware;

use App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

class CorsMiddleware
{
    public function __invoke(Request $request, RequestHandler $handler): Response
    {
        if ($request->getMethod() === 'OPTIONS') {
            $response = new \Slim\Psr7\Response();
            return $this->addCorsHeaders($response);
        }
        $response = $handler->handle($request);
        return $this->addCorsHeaders($response);
    }

    private function addCorsHeaders(Response $response): Response
    {
        return $response
            ->withHeader('Access-Control-Allow-Origin', App::$allowedAccessOrigins)
            ->withHeader('Access-Control-Allow-Methods', App::$allowedAccessMethods)
            ->withHeader('Access-Control-Allow-Headers', App::$allowedAccessHeaders)
            ->withHeader('Access-Control-Max-Age', App::$allowedAccessMaxAge)
            ->withHeader('Access-Control-Expose-Headers', App::$allowedExposeHeaders);
    }
}
