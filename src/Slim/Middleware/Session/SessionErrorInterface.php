<?php

namespace BO\Slim\Middleware\Session;

interface SessionErrorInterface
{
    const IS_EMPTY_ERROR = 'isEmpty';

    const IS_DIFFERENT_ERROR = 'isDifferent';

    const IS_VERIFIED_ERROR = 'isVerified';

    const IS_RELOAD_ERROR = 'isStepReloadForbidden';

    const IS_FINSIHED_ERROR = 'isFinished';

    const IS_OVERAGED_ERROR = 'isOveraged';

    const HAS_NO_RROCESS_ERROR = 'hasNoProcess';

    const HAS_NO_DATE_ERROR = 'hasNoDate';


    public static function isEmpty(string $redirect, array $params);

    public static function isDifferent(string $redirect, array $params);

    public static function isVerified(string $redirect, array $steps, string $referer);

    public static function isStepReloadForbidden(string $redirect, string $step, int $maxReload = 1);

    public static function isObsolete(string $redirect, array $params);

    public static function isOveraged(string $redirect, array $params);
    
    public static function hasNoProcess(string $redirect, array $params);

    public static function hasNoDate(string $redirect, array $params);
}
